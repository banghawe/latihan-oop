// Class

class Person{
    constructor(name, address){
        this.name = name;
        this.address = address;
    }

    introduce(){
        console.log(`Saya bernama ${this.name} dari ${this.address}`);
    }
}

class Librarian extends Person{
    activate(member){
        if(member.getMemberStatus){
            console.log(`You're already a member!`)
        }else{
            member.setMemberStatus(1);
            console.log('You are now a member!')
        }
    }

    deactivate(member){
        if(member.getMemberStatus){
            member.setMemberStatus(0);
            console.log(`You're now not a member!`)
        }else{
            console.log(`You're not a member!`)
        }
    }

    checkout(book, member){
        if(member.getMemberStatus()){
            if(book.getBorrowedStatus()){
                if(member.getBorrowedBook() < 2){
                    member.setBorrowedBook(1);
                    book.setBorrowedStatus(0);
                }else{
                    console.log('You are already borrowed 2 books');
                }
            }else{
                console.log('Book is not available');
            }
        }else{
            console.log('You are not a member');
        }
    }

    return(book, member){
        book.setBorrowedStatus(1);
        member.setBorrowedBook(-1);
    }
}

class Member extends Person{
    constructor(name, address){
        super(name, address);
        this.memberStatus = 0;
        this.borrowedBooks = 0;
    }

    setMemberStatus(status){
        this.memberStatus = status;
    }

    getMemberStatus(){
        return this.memberStatus;
    }

    setBorrowedBook(status){
        this.borrowedBooks += status;
    }

    getBorrowedBook(){
        return this.borrowedBooks;
    }
}

class Book{
    constructor(title, isbn, writer){
        this.title = title;
        this.isbn = isbn;
        this.writer = writer;
        this.borrowedStatus = 1; //1 avail - 0 borrowed
    }

    setBorrowedStatus(status){
        this.borrowedStatus = status;
    }

    getBorrowedStatus(){
        return this.borrowedStatus;
    }
}

let hendra = new Librarian('Hendra', 'Jogja');
let nico = new Member('Nico', 'Jakarta');

let abcBook = new Book('ABC', '1111', 'Joni');
let defBook = new Book('DEF', '2222', 'Jonik');
let ghiBook = new Book('GHI', '3333', 'Jonin');

hendra.introduce();
nico.introduce();

hendra.checkout(abcBook, nico);